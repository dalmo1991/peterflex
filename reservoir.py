class Reservoir():

    def __init__(self, parameters, state, solver):
        self._parameters = parameters
        self._state = state
        self._solver = solver

    def set_input(self, input):
        self._input = {'P': input[0]}

    def get_output(self, timestamps):
        state = self._solver.solve(
            flux_fun=self._fluxes,
            time=timestamps,
            S0=self._state,
            **self._input,
            **self._parameters,
        )

        # It is more complicated
        return self._solver.calculate_flux(state=state, index=1)

    @staticmethod
    def _fluxes(S, P, k, alpha):

        return [
            P,
            k*S**alpha,
        ]